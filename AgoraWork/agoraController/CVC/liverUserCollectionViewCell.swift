//
//  liverUserCollectionViewCell.swift
//  zarathx
//
//  Created by Junaid  Kamoka on 14/12/2020.
//  Copyright © 2020 Junaid Kamoka. All rights reserved.
//

import UIKit

class liverUserCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var imgUser: UIImageView!
}
