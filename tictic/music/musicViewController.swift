//
//  musicViewController.swift
//  MusicTok
//
//  Created by Admin on 20/06/21.
//  Copyright © 2021 Junaid Kamoka. All rights reserved.
//

import UIKit
import MediaPlayer
//import IQMediaPickerController

class musicViewController: UIViewController, UITabBarControllerDelegate , IQAudioPickerControllerDelegate{
    
    func audioPickerController(_ mediaPicker: IQAudioPickerController, didPick mediaItems: [MPMediaItem]) {
        print(mediaItems)
    }
    
    func audioPickerControllerDidCancel(_ mediaPicker: IQAudioPickerController) {
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func openMusicBtn(_ sender: UIButton){
        let media = IQAudioPickerController()
        media.delegate = self
           present(media, animated: true) {
        }
    }
}

//
//extension musicViewController: IQMediaPickerControllerDelegate ,UINavigationControllerDelegate  {
//    func mediaPickerController(_ controller: IQMediaPickerController, didFinishMedias selection: IQMediaPickerSelection) {
//
//
//    }
//
//    func mediaPickerControllerDidCancel(_ controller: IQMediaPickerController) {
//
//
//}
